run: build
	ENV=dev ./test2

build: generate
	go build -o test2 ./service

generate:
	go generate $$(go list ./...)

setup-test: generate
	go get github.com/vektra/mockery/.../

test:
	go test ./...
