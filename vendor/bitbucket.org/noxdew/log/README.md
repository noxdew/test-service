# noxdew/log

A simple log initialisation library for [zap](https://github.com/uber-go/zap)

This library is integrated with [noxdew/config](https://bitbucket.org/noxdew/config)

## Installation

`go get bitbucket.org/noxdew/log`

## Usage

```go
import "bitbucket.org/noxdew/log"

func main() {
    logConfig := log.Config{} // Or parse yaml/json file into it
    logger := logConfig.Create()

    logger.Info(...)
}
```

Note: the `Create` method panics on all exceptions. This is to prevent a service/application to run without proper configuration

## Contributing

Feel free to create issues or contribute code. We are reviewing them regularly.
