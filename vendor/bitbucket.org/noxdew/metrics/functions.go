package metrics

import (
	"encoding/json"
	"time"

	metrics "github.com/rcrowley/go-metrics"
	zap "go.uber.org/zap"
)

// Increment the specified counter by the value
// Counters hold an int64 value that can be incremented and decremented
func Increment(path string, value int64) {
	IncrementWithTags(path, value, nil)
}

// IncrementWithTags the specified counter by the value with the given tags
// Counters hold an int64 value that can be incremented and decremented
func IncrementWithTags(path string, value int64, tags map[string]string) {
	metrics.GetOrRegisterCounter(mergeNameAndTags(path+".counter", tags), metrics.DefaultRegistry).Inc(value)
}

// IncrementBy1 the specified counter
// Counters hold an int64 value that can be incremented and decremented
func IncrementBy1(path string) {
	Increment(path, 1)
}

// IncrementBy1WithTags the specified counter with the given tags
// Counters hold an int64 value that can be incremented and decremented
func IncrementBy1WithTags(path string, tags map[string]string) {
	IncrementWithTags(path, 1, tags)
}

// Decrement the specified counter by the value
// Counters hold an int64 value that can be incremented and decremented
func Decrement(path string, value int64) {
	DecrementWithTags(path, value, nil)
}

// DecrementWithTags the specified counter by the value with the given tags
// Counters hold an int64 value that can be incremented and decremented
func DecrementWithTags(path string, value int64, tags map[string]string) {
	metrics.GetOrRegisterCounter(mergeNameAndTags(path+".counter", tags), metrics.DefaultRegistry).Dec(value)
}

// DecrementBy1 the specified counter
// Counters hold an int64 value that can be incremented and decremented
func DecrementBy1(path string) {
	Decrement(path, 1)
}

// DecrementBy1WithTags the specified counter with the given tags
// Counters hold an int64 value that can be incremented and decremented
func DecrementBy1WithTags(path string, tags map[string]string) {
	DecrementWithTags(path, 1, tags)
}

// UpdateGauge records the value on the specified gauge
// Gauges hold an int64 value that can be set arbitrarily
func UpdateGauge(path string, value int64) {
	UpdateGaugeWithTags(path, value, nil)
}

// UpdateGaugeWithTags records the value on the specified gauge with the given tags
// Gauges hold an int64 value that can be set arbitrarily
func UpdateGaugeWithTags(path string, value int64, tags map[string]string) {
	metrics.GetOrRegisterGauge(mergeNameAndTags(path+".gauge", tags), metrics.DefaultRegistry).Update(value)
}

// UpdateGaugeFloat64 records the value on the specified gauge
// Gauges hold an float64 value that can be set arbitrarily
func UpdateGaugeFloat64(path string, value float64) {
	UpdateGaugeFloat64WithTags(path, value, nil)
}

// UpdateGaugeFloat64WithTags records the value on the specified gauge with the given tags
// Gauges hold an float64 value that can be set arbitrarily
func UpdateGaugeFloat64WithTags(path string, value float64, tags map[string]string) {
	metrics.GetOrRegisterGaugeFloat64(mergeNameAndTags(path+".gauge", tags), metrics.DefaultRegistry).Update(value)
}

// UpdateHistogram records the value against the specified histogram
// Histograms calculate distribution statistics from a series of int64 values
func UpdateHistogram(path string, value int64) {
	UpdateHistogramWithTags(path, value, nil)
}

// UpdateHistogramWithTags records the value against the specified histogram with the given tags
// Histograms calculate distribution statistics from a series of int64 values
func UpdateHistogramWithTags(path string, value int64, tags map[string]string) {
	// The following syntax is used as the sample needs to be provided to GetOrRegister
	// This however means that a new instance of the sample would be created every time
	// even when the histogram already exists. This GetOrRegister on the registry allows
	// passing in a function which will be executed only when the histogram doesn't exist
	metrics.DefaultRegistry.GetOrRegister(mergeNameAndTags(path+".histogram", tags), func() metrics.Histogram {
		return metrics.NewHistogram(NewSlidingWindowSample(time.Minute))
	}).(metrics.Histogram).Update(value)
}

// MarkMeter records the value against the specified meter
// Meters count events to produce exponentially-weighted moving average rates at one-, five-, and fifteen-minutes and a mean rate
func MarkMeter(path string, value int64) {
	MarkMeterWithTags(path, value, nil)
}

// MarkMeterWithTags records the value against the specified meter with the given tags
// Meters count events to produce exponentially-weighted moving average rates at one-, five-, and fifteen-minutes and a mean rate
func MarkMeterWithTags(path string, value int64, tags map[string]string) {
	metrics.GetOrRegisterMeter(mergeNameAndTags(path+".meter", tags), metrics.DefaultRegistry).Mark(value)
}

// Time record the duration of execution of the given function
// Timers capture the duration and rate of events
func Time(path string, f func()) {
	TimeWithTags(path, nil, f)
}

// TimeWithTags record the duration of execution of the given function with the given tags
// Timers capture the duration and rate of events
func TimeWithTags(path string, tags map[string]string, f func()) {
	metrics.GetOrRegisterTimer(mergeNameAndTags(path+".timer", tags), metrics.DefaultRegistry).Time(f)
}

// UpdateTimer records the duration against the specified timer
// Timers capture the duration and rate of events
func UpdateTimer(path string, duration time.Duration) {
	UpdateTimerWithTags(path, duration, nil)
}

// UpdateTimerWithTags records the duration against the specified timer with the given tags
// Timers capture the duration and rate of events
func UpdateTimerWithTags(path string, duration time.Duration, tags map[string]string) {
	metrics.GetOrRegisterTimer(mergeNameAndTags(path+".timer", tags), metrics.DefaultRegistry).Update(duration)
}

// UpdateTimerSince records the elapsed time since <start> against the specified timer
// Timers capture the duration and rate of events
func UpdateTimerSince(path string, start time.Time) {
	UpdateTimerSinceWithTags(path, start, nil)
}

// UpdateTimerSinceWithTags records the elapsed time since <start> against the specified timer with the given tags
// Timers capture the duration and rate of events
func UpdateTimerSinceWithTags(path string, start time.Time, tags map[string]string) {
	metrics.GetOrRegisterTimer(mergeNameAndTags(path+".timer", tags), metrics.DefaultRegistry).UpdateSince(start)
}

func mergeNameAndTags(name string, tags map[string]string) string {
	if tags == nil {
		return name
	}

	bytesTags, err := json.Marshal(tags)
	if err != nil {
		zap.L().Error("Failed to serialise tags",
			zap.String("metric", name),
			zap.Error(err),
		)
	}

	return name + ";;;" + string(bytesTags)
}
