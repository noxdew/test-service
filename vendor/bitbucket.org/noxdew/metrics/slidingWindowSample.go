package metrics

import (
	"container/list"
	"math"
	"sort"
	"sync"
	"time"

	metrics "github.com/rcrowley/go-metrics"
)

var samplesMutex sync.RWMutex
var samples = make([]SlidingWindowSample, 0)
var minimumSize = 1024

// SlidingWindowSample is an implementation of the sliding window algorithm
type SlidingWindowSample struct {
	mutex  *sync.RWMutex
	list   *list.List
	window time.Duration
}

type samplePair struct {
	value int64
	time  int64
}

// NewSlidingWindowSample constructs a new sliding window sample with the given window
func NewSlidingWindowSample(window time.Duration) metrics.Sample {
	sample := SlidingWindowSample{
		list:   list.New(),
		window: window,
		mutex:  new(sync.RWMutex),
	}
	samplesMutex.Lock()
	defer samplesMutex.Unlock()
	samples = append(samples, sample)
	return &sample
}

// Clear clears all samples.
func (s *SlidingWindowSample) Clear() {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.list = list.New()
}

// Count returns the number of samples recorded.
func (s *SlidingWindowSample) Count() int64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return int64(s.list.Len())
}

// Max returns the maximum value in the sample, which may not be the maximum
// value ever to be part of the sample.
func (s *SlidingWindowSample) Max() int64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return sampleMax(s.list)
}

// Mean returns the mean of the values in the sample.
func (s *SlidingWindowSample) Mean() float64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return sampleMean(s.list)
}

// Min returns the minimum value in the sample, which may not be the minimum
// value ever to be part of the sample.
func (s *SlidingWindowSample) Min() int64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return sampleMin(s.list)
}

// Percentile returns an arbitrary percentile of values in the sample.
func (s *SlidingWindowSample) Percentile(p float64) float64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return samplePercentile(s.list, p)
}

// Percentiles returns a slice of arbitrary percentiles of values in the sample.
func (s *SlidingWindowSample) Percentiles(ps []float64) []float64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return samplePercentiles(s.list, ps)
}

// Size returns the size of the sample.
func (s *SlidingWindowSample) Size() int {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return s.list.Len()
}

// Snapshot returns a read-only copy of the sample.
func (s *SlidingWindowSample) Snapshot() metrics.Sample {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return metrics.NewSampleSnapshot(int64(s.list.Len()), s.Values())
}

// StdDev returns the standard deviation of the values in the sample.
func (s *SlidingWindowSample) StdDev() float64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return sampleStdDev(s.list)
}

// Sum returns the sum of the values in the sample.
func (s *SlidingWindowSample) Sum() int64 {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return sampleSum(s.list)
}

// Update samples a new value.
func (s *SlidingWindowSample) Update(v int64) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.list.PushBack(samplePair{
		value: v,
		time:  time.Now().UnixNano(),
	})
}

// Values returns a copy of the values in the sample.
func (s *SlidingWindowSample) Values() []int64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	values := make([]int64, s.list.Len())
	for e := s.list.Front(); e != nil; e = e.Next() {
		values = append(values, (e.Value.(samplePair)).value)
	}
	return values
}

// Variance returns the variance of the values in the sample.
func (s *SlidingWindowSample) Variance() float64 {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return sampleVariance(s.list)
}

// removes all values outside of the window
func (s *SlidingWindowSample) trim(now int64) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	border := now - s.window.Nanoseconds()
	for e := s.list.Front(); e != nil; {
		pair := e.Value.(samplePair)
		toBeRemoved := e
		e = e.Next()
		if pair.time < border {
			s.list.Remove(toBeRemoved)
		} else {
			break
		}
	}
}

type int64Slice []int64

func (p int64Slice) Len() int           { return len(p) }
func (p int64Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p int64Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

//// Helpers for handling the structs directly
// returns the maximum value of the slice of int64.
func sampleMax(values *list.List) int64 {
	if 0 == values.Len() {
		return 0
	}
	var max int64 = math.MinInt64
	for e := values.Front(); e != nil; e = e.Next() {
		val := (e.Value.(samplePair)).value
		if max < val {
			max = val
		}
	}
	return max
}

// returns the mean value of the slice of int64.
func sampleMean(values *list.List) float64 {
	if 0 == values.Len() {
		return 0.0
	}
	return float64(sampleSum(values)) / float64(values.Len())
}

// returns the minimum value of the slice of int64.
func sampleMin(values *list.List) int64 {
	if 0 == values.Len() {
		return 0
	}
	var min int64 = math.MaxInt64
	for e := values.Front(); e != nil; e = e.Next() {
		val := (e.Value.(samplePair)).value
		if min > val {
			min = val
		}
	}
	return min
}

// returns an arbitrary percentile of the slice of int64.
func samplePercentile(list *list.List, p float64) float64 {
	return samplePercentiles(list, []float64{p})[0]
}

// returns a slice of arbitrary percentiles of the slice
func samplePercentiles(list *list.List, ps []float64) []float64 {
	scores := make([]float64, len(ps))
	size := list.Len()
	// extract the values
	values := make(int64Slice, list.Len())
	for e := list.Front(); e != nil; e = e.Next() {
		values = append(values, (e.Value.(samplePair)).value)
	}

	if size > 0 {
		sort.Sort(values)
		for i, p := range ps {
			pos := p * float64(size+1)
			if pos < 1.0 {
				scores[i] = float64(values[0])
			} else if pos >= float64(size) {
				scores[i] = float64(values[size-1])
			} else {
				lower := float64(values[int(pos)-1])
				upper := float64(values[int(pos)])
				scores[i] = lower + (pos-math.Floor(pos))*(upper-lower)
			}
		}
	}
	return scores
}

// returns the standard deviation of the slice of int64.
func sampleStdDev(list *list.List) float64 {
	return math.Sqrt(sampleVariance(list))
}

// returns the sum of the slice of int64.
func sampleSum(list *list.List) int64 {
	var sum int64
	for e := list.Front(); e != nil; e = e.Next() {
		sum += (e.Value.(samplePair)).value
	}
	return sum
}

// returns the variance of the slice of int64.
func sampleVariance(list *list.List) float64 {
	if 0 == list.Len() {
		return 0.0
	}
	m := sampleMean(list)
	var sum float64
	for e := list.Front(); e != nil; e = e.Next() {
		d := float64((e.Value.(samplePair)).value) - m
		sum += d * d
	}
	return sum / float64(list.Len())
}

// StartSlidingWindowTrimmer starts the regular trimming of sliding window samples
func StartSlidingWindowTrimmer() {
	ticker := time.NewTicker(time.Second)
	for range ticker.C {
		trimData()
	}
}

func trimData() {
	samplesMutex.RLock()
	defer samplesMutex.RUnlock()

	for _, sample := range samples {
		now := time.Now().UnixNano()
		sample.trim(now)
	}
}
