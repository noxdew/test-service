# metrics

A metrics library for GoLang services and applications

This library is integrated with [noxdew/config](https://bitbucket.org/noxdew/config)

## Installation

`go get bitbucket.org/noxdew/metrics`

## Usage

```go
import "bitbucket.org/noxdew/metrics"

func main() {
    metricsConfig := metrics.Config{} // Or parse yaml/json file into it
    metricsConfig.Configure(logger)

    // Check all available functions in functions.go
    metrics.IncrementBy1("some.path")
}
```

## Currently supported reporters

- `none` - no reported, essentially disabled metrics
- `log` - log all reports to `stderr`, perfect for development
- `influxdb` - send metrics to InfluxDB

If you need a new reporter, feel free to submit a PR or request it in an issue. We will try to add more.

## Contributing

Feel free to create issues or contribute code. We are reviewing them regularly.
