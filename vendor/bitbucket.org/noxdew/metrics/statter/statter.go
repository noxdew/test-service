package statter

import (
	"time"

	metrics "bitbucket.org/noxdew/metrics"
)

// MetricsStatter implementation of a statter using go-metrics
type MetricsStatter struct{}

// Inc increments a counter
func (ms MetricsStatter) Inc(metric string, val int64, rate float32) error {
	metrics.Increment(metric, val)
	return nil
}

// TimingDuration records a duration of an event in nanoseconds
func (ms MetricsStatter) TimingDuration(metric string, val time.Duration, rate float32) error {
	metrics.UpdateTimer(metric, val)
	return nil
}
