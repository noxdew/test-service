package metrics

import (
	"encoding/json"
	"net/url"
	"strings"
	"time"

	influxdb "github.com/influxdata/influxdb/client"
	metrics "github.com/rcrowley/go-metrics"
	zap "go.uber.org/zap"
)

type reporter struct {
	reg      metrics.Registry
	interval time.Duration

	url      url.URL
	database string
	username string
	password string
	tags     map[string]string

	client *influxdb.Client

	logger *zap.Logger
}

// InfluxDB starts a InfluxDB reporter which will post the metrics from the given registry at each d interval with the specified tags
func InfluxDB(r metrics.Registry, config ReporterConfig, logger *zap.Logger) {
	u, err := url.Parse(config.URL)
	if err != nil {
		logger.Panic("Failed to parse InfluxDB url",
			zap.String("url", config.URL),
			zap.Error(err),
		)
	}

	rep := &reporter{
		reg:      r,
		interval: time.Duration(config.Interval) * time.Second,
		url:      *u,
		database: config.Database,
		username: config.Username,
		password: config.Password,
		tags:     config.Tags,
		logger:   logger,
	}
	if err := rep.makeClient(); err != nil {
		logger.Panic("Failed to make InfluxDB client",
			zap.Error(err),
		)
	}

	rep.run()
}

func (r *reporter) makeClient() (err error) {
	r.client, err = influxdb.NewClient(influxdb.Config{
		URL:      r.url,
		Username: r.username,
		Password: r.password,
	})
	return
}

func (r *reporter) run() {
	intervalTicker := time.Tick(r.interval)
	pingTicker := time.Tick(time.Second * 5)

	for {
		select {
		case <-intervalTicker:
			if err := r.send(); err != nil {
				r.logger.Error("Unable to send metrics to InfluxDB",
					zap.Error(err),
				)
			}
		case <-pingTicker:
			_, _, err := r.client.Ping()
			if err != nil {
				r.logger.Error("Failed to ping InfluxDB, trying to recreate client",
					zap.Error(err),
				)

				if err = r.makeClient(); err != nil {
					r.logger.Panic("Failed to make InfluxDB client",
						zap.Error(err),
					)
				}
			}
		}
	}
}

func (r *reporter) send() error {
	var pts []influxdb.Point

	r.reg.Each(func(name string, i interface{}) {
		now := time.Now()

		name, tags := r.extractTags(name)
		merge(tags, r.tags)

		switch metric := i.(type) {
		case metrics.Counter:
			ms := metric.Snapshot()
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"value": ms.Count(),
				},
				Time: now,
			})
		case metrics.Gauge:
			ms := metric.Snapshot()
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"value": ms.Value(),
				},
				Time: now,
			})
		case metrics.GaugeFloat64:
			ms := metric.Snapshot()
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"value": ms.Value(),
				},
				Time: now,
			})
		case metrics.Histogram:
			ms := metric.Snapshot()
			ps := ms.Percentiles([]float64{0.5, 0.75, 0.95, 0.99, 0.999, 0.9999})
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"count":    ms.Count(),
					"max":      ms.Max(),
					"mean":     ms.Mean(),
					"min":      ms.Min(),
					"stddev":   ms.StdDev(),
					"variance": ms.Variance(),
					"p50":      ps[0],
					"p75":      ps[1],
					"p95":      ps[2],
					"p99":      ps[3],
					"p999":     ps[4],
					"p9999":    ps[5],
				},
				Time: now,
			})
		case metrics.Meter:
			ms := metric.Snapshot()
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"count": ms.Count(),
					"m1":    ms.Rate1(),
					"m5":    ms.Rate5(),
					"m15":   ms.Rate15(),
					"mean":  ms.RateMean(),
				},
				Time: now,
			})
		case metrics.Timer:
			ms := metric.Snapshot()
			ps := ms.Percentiles([]float64{0.5, 0.75, 0.95, 0.99, 0.999, 0.9999})
			pts = append(pts, influxdb.Point{
				Measurement: name,
				Tags:        tags,
				Fields: map[string]interface{}{
					"count":    ms.Count(),
					"max":      ms.Max(),
					"mean":     ms.Mean(),
					"min":      ms.Min(),
					"stddev":   ms.StdDev(),
					"variance": ms.Variance(),
					"p50":      ps[0],
					"p75":      ps[1],
					"p95":      ps[2],
					"p99":      ps[3],
					"p999":     ps[4],
					"p9999":    ps[5],
					"m1":       ms.Rate1(),
					"m5":       ms.Rate5(),
					"m15":      ms.Rate15(),
					"meanrate": ms.RateMean(),
				},
				Time: now,
			})
		}
	})

	bps := influxdb.BatchPoints{
		Points:   pts,
		Database: r.database,
	}

	_, err := r.client.Write(bps)
	return err
}

func (r *reporter) extractTags(name string) (string, map[string]string) {
	split := strings.SplitN(name, ";;;", 2)
	if len(split) < 2 {
		return split[0], make(map[string]string)
	}

	tags := make(map[string]string)
	err := json.Unmarshal([]byte(split[1]), &tags)
	if err != nil {
		r.logger.Error("Failed to parse metric tags",
			zap.String("metric", split[0]),
			zap.Error(err),
		)
		// we don't want to panic ot block, just send the metric,
		// the service has been running for some time
		return split[0], make(map[string]string)
	}

	return split[0], tags
}

func merge(destination, origin map[string]string) {
	if destination == nil || origin == nil {
		return
	}
	for k, v := range origin {
		destination[k] = v
	}
}
