package metrics

import (
	"log"
	"os"
	"time"

	metrics "github.com/rcrowley/go-metrics"
	zap "go.uber.org/zap"
)

// Config represent the configuration of the metrics package
type Config struct {
	Type           string         `yaml:"type" json:"type"`
	ReporterConfig ReporterConfig `yaml:"config" json:"config"`
}

// ReporterConfig represents the configuration of the reporter
type ReporterConfig struct {
	URL      string            `yaml:"url" json:"url"`
	Username string            `yaml:"username" json:"username"`
	Password string            `yaml:"password" json:"password"`
	Database string            `yaml:"database" json:"database"`
	Prefix   string            `yaml:"prefix" json:"prefix"`
	Interval int               `yaml:"interval" json:"interval"` // in seconds
	Tags     map[string]string `yaml:"tags" json:"tags"`
}

// Configure go-metrics using the config of the service
// it panics on all errors as it is better than running the service in the dark
func (config Config) Configure(logger *zap.Logger) {
	if logger == nil {
		logger = zap.L()
	}

	if config.Type == "none" {
		// Don't init any reporters
	} else if config.Type == "log" {
		// Adding sensible defaults
		if config.ReporterConfig.Interval == 0 {
			config.ReporterConfig.Interval = 5
		}
		go metrics.Log(metrics.DefaultRegistry, time.Duration(config.ReporterConfig.Interval)*time.Second, log.New(os.Stderr, config.ReporterConfig.Prefix, log.Lmicroseconds))
	} else if config.Type == "influxdb" {
		// Adding sensible defaults
		if config.ReporterConfig.Interval == 0 {
			config.ReporterConfig.Interval = 1
		}
		go InfluxDB(metrics.DefaultRegistry, config.ReporterConfig, logger)
	} else {
		logger.Panic("Failed to create logger",
			zap.Strings("valid", []string{"log", "influxdb"}),
			zap.String("type", config.Type),
			zap.String("action", "Contact us at team@noxdew.com and we will add support for the sink you need :)"),
		)
	}

	go StartSlidingWindowTrimmer()
}
