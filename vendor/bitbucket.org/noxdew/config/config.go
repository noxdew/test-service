package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	log "bitbucket.org/noxdew/log"
	metrics "bitbucket.org/noxdew/metrics"
	mapstruct "github.com/mitchellh/mapstructure"
	zap "go.uber.org/zap"
	yaml "gopkg.in/yaml.v2"
)

type tlsConfig struct {
	Key  string `yaml:"key" json:"key"`
	Cert string `yaml:"cert" json:"cert"`
}

// Config struct containing the structure of the config file
type Config struct {
	ServiceName string                 `yaml:"serviceName" json:"serviceName"`
	Env         string                 `yaml:"env" json:"env"`
	Port        int                    `yaml:"port" json:"port"`
	Logging     log.Config             `yaml:"logging" json:"logging"`
	Metrics     metrics.Config         `yaml:"metrics" json:"metrics"`
	TLS         tlsConfig              `yaml:"tls" json:"tls"`
	Custom      map[string]interface{} `yaml:"custom" json:"custom"`
}

type accessor interface {
	Getenv(key string) string
	ReadFile(path string) ([]byte, error)
	Exists(path string) bool
}

type parser interface {
	Unmarshal([]byte, interface{}) error
}

type fsAccessor struct{}

func (fsAccessor) Getenv(key string) string {
	return os.Getenv(key)
}

func (fsAccessor) Exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func (fsAccessor) ReadFile(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

type yamlParser struct{}

func (yamlParser) Unmarshal(data []byte, dest interface{}) error {
	return yaml.Unmarshal(data, dest)
}

type jsonParser struct{}

func (jsonParser) Unmarshal(data []byte, dest interface{}) error {
	return json.Unmarshal(data, dest)
}

// Load reads the appropriate config file and creates the config object
func Load(customConfig interface{}) Config {
	return load(fsAccessor{}, customConfig)
}

func resolveFilePathAndParser(ac accessor) (env string, path string, p parser) {
	env = ac.Getenv("ENV")
	if env == "" {
		// default to prod, it is best to always explicitly define the environment,
		// however it is better to default to prod in case a problem occurs and the env is missing in prod
		env = "prod"
	}

	path = "config/" + env

	if ac.Exists(path + ".yml") {
		return env, path + ".yml", yamlParser{}
	}
	if ac.Exists(path + ".json") {
		return env, path + ".json", jsonParser{}
	}

	zap.L().Panic("Failed to find config file",
		zap.String("env", env),
		zap.Strings("tried", []string{path + ".yml", path + ".json"}),
	)
	// This will never be reached as the logged will always panic
	return "", "", nil
}

func load(ac accessor, customConfig interface{}) Config {
	env, path, p := resolveFilePathAndParser(ac)

	confData, err := ac.ReadFile(path)
	if err != nil {
		zap.L().Panic("Failed to load config",
			zap.String("env", env),
			zap.String("path", path),
			zap.Error(err),
		)
	}

	conf := Config{}
	err = p.Unmarshal(confData, &conf)
	if err != nil {
		zap.L().Panic("Failed to parse config",
			zap.String("env", env),
			zap.String("path", path),
			zap.Error(err),
		)
	}

	if customConfig != nil {
		err = mapstruct.Decode(conf.Custom, &customConfig)
		if err != nil {
			zap.L().Panic("Failed to decode custom config",
				zap.Error(err),
			)
		}
	}

	conf.Env = env
	return conf
}
