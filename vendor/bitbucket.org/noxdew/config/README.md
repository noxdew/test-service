# config

A library for reading and parsing yaml config .

This library is integrated with [noxdew/log](https://bitbucket.org/noxdew/log) and [noxdew/metrics](https://bitbucket.org/noxdew/metrics) and parses their configs by default.

The library reads config from `config/<env>.yml` or `config/<env>.json` where `env` is `$ENV`. This way you can have separate config files in the repository for development, testing, production and any other environments in your pipeline.

Note: It uses [zap](https://github.com/uber-go/zap) for logging. Since it parses the config for the logger it panics on all errors and uses the global logger.

## Installation

`go get bitbucket.org/noxdew/config`

## Usage

```go
import "bitbucket.org/noxdew/config"

func main() {
    // Any object defining your custom schema, it will be populated from the data in the "custom" top level property
    customConfig := CustomConfig{}
    conf := config.Load(customConfig)

    // Then you can call the initialisation code for noxdew/log and noxdew/metrics
    logger := conf.Logging.Create()
    conf.Metrics.Configure(logger)
}
```

## Contributing

Feel free to create issues or contribute code. We are reviewing them regularly.
